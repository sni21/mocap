ROS Motion Capture Drivers
==========================

This package contains ROS wrappers for VICON and OptiTrack drivers. It allows the pose of multiple rigid bodies to be broadcast over the ROS network at the rate specified by underlying software (VICON Tracker or OptiTrack Motive).

In addition to streaming the pose of rigid bodies, this packages can stream twist and linear acceleration. To do so, a quaternion-based attitude EKF estimates `omega` and `q` from input measurements `q_mocap` and a GHK filter (nearly-constant acceleration model) is used to generate `p`, `v`, and `a` from input measurements `p_mocap`.

## Getting Started

Clone this package into your workspace and build (e.g., `catkin build`).

### Networking

The motion capture system is run on a Windows machine. This Windows machine and the ROS basestation (i.e., `sikorsky`) must be on the same network.

Since the motion capture software uses multicast, this feature must be enabled for the specific network interface card. These stanzas are from `/etc/network/interfaces` on `sikorsky` (Jan 2020):

```bash
# For vicon
auto enp4s0
iface enp4s0 inet static
address 192.168.0.19
post-up ifconfig enp4s0 multicast; route add -net 224.0.0.0 netmask 240.0.0.0 dev enp4s0
netmask 255.255.255.0

# FAST Lab OptiTrack Mocal Connection
auto enp0s31f6
iface enp0s31f6 inet static
address 192.168.1.13
post-up ifconfig enp0s31f6 multicast; route add -net 224.0.0.0 netmask 240.0.0.0 dev enp0s31f6
netmask 255.255.255.0
```

### Running

1. Turn on motion capture system
2. Start mocap software on Windows computer (i.e., VICON Tracker or OptiTrack Motive)
3. Run `roslaunch mocap vicon.launch` or `roslaunch mocap optitrack.launch`

## Room Bounds

Since motion capture understands the valid flight volume, it makes since to have this package also set the global room bounds. These room bounds are set on the `rosparam` server in the `/room_bounds` namespace. Other packages should respect these bounds by not allowing robots to leave them.

## Coordinate Frames
*The hardest thing in robotics*

ROS uses an East-North-Up (ENU) inertial coordinate frame. In the ACL Highbay, we have defined our `world` coordinate frame to have *x* aligned with the length of the highbay, positvely pointing East. The `world` *y* is aligned with the width of the room, positively pointing North (i.e., pointing away from the control room). The *z* coordinate is pointing up.

VICON and OptiTrack have their own coordinate frames, which will not necessarily be the same as we've defined our `world` frame above. To resolve this difference, we must define a transformation from VICON / OptiTrack into the `world`. Note that this transformation does not show up in the ROS tf tree---it happens behind the scenes whenever raw mocap measurements are received. This transformation is defined in each launch file as `mocap_wrt_parent_frame`
(and `body_wrt_mocap_body`---if you are unsure, more than likely this should be set as the inverse of the `mocap_wrt_parent_frame` transform).

Once these transformations are defined you never need to think of the raw mocap coordinate frame again. Instead, think of everything as defined in the `parent` (i.e., `world`) frame.

## Implementation Details

There are three main components of this software:

1. The underlying motion capture client SDK, which retrieves raw multicast packets from the mocap software.
2. The ACL rigid body filtering to produce 6 DOF ground-truth state.
3. The ROS wrapper, which drives the client SDK to receive data, manages a list of rigid bodies, and provides measurement updates to existing rigid bodies.

## Motion Capture Software Settings (Windows)

### VICON Tracker

We depend on an accurate update rate from VICON. Make sure the **Genlock and Timecode** settings match with the following image. These settings correspond with `ViconSDK::TimecodeStandard::PAL`.

![tracker2.2_timecode_settings](.gitlab/timecode_settings.png)

### OptiTrack Motive

## Creating Rigid Bodies

### [VICON Tracker](https://gitlab.com/mit-acl/fsw/mocap/-/wikis/VICON:-Creating-Rigid-Bodies)

### OptiTrack Motive

## FAQ

1. What frame are twist and accel published in?

    - All signals are w.r.t the `world` frame. Twist and accel are expressed in the `world` frame, **not** the body frame.

2. If a rigid body becomes occluded, will its pose still be broadcast?

    - No. Signals associated with a rigid body are only broadcast when a valid raw mocap measurement is received.

3. What version of VICON Tracker are we using?

    - We use version 2.2 of the software / API.
