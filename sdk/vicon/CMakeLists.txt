cmake_minimum_required(VERSION 2.8.3)
project(vicon_sdk)

add_library(vicon_sdk SHARED IMPORTED GLOBAL)
set_property(TARGET vicon_sdk PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${CMAKE_CURRENT_SOURCE_DIR})
set_property(TARGET vicon_sdk PROPERTY IMPORTED_LOCATION ${CMAKE_CURRENT_SOURCE_DIR}/libViconDataStreamSDK_CPP.so)
