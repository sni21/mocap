/**
 * @file vicon.cpp
 * @brief Specialization for VICON client
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 11 Dec 2019
 */

#include "mocap/client/vicon.h"

namespace acl {
namespace mocap {

VICON::VICON(const Parameters& params)
: params_(params)
{}

// ----------------------------------------------------------------------------

VICON::~VICON()
{
  client_.Disconnect();
}

// ----------------------------------------------------------------------------

bool VICON::init()
{
  // attempt to connect to server
  client_.Connect(params_.host);
  if (!client_.IsConnected().Connected) return false;

  // enable useful data streams
  client_.EnableSegmentData();
  client_.EnableMarkerData();
  client_.EnableUnlabeledMarkerData();
  client_.EnableDeviceData();

  // set the streaming mode
  // client_.SetStreamMode( ViconSDK::StreamMode::ClientPull );
  // client_.SetStreamMode( ViconSDK::StreamMode::ClientPullPreFetch );
  client_.SetStreamMode(ViconSDK::StreamMode::ServerPush); // this is the lowest latency version

  // Set the global up axis (Z-up)
  client_.SetAxisMapping(ViconSDK::Direction::Forward,
                          ViconSDK::Direction::Left, ViconSDK::Direction::Up);

  // Skip some frames to avoid the "leftover" subject names from last conneciton.
  // This is perhaps due to the ServerPush mode.
  for (int i=0; i<25; i++) {
      while (client_.GetFrame().Result != ViconSDK::Result::Success);
  }

  return client_.IsConnected().Connected;
}

// ----------------------------------------------------------------------------

bool VICON::spinOnce()
{
  // make sure that there is a new frame of data to consume
  return client_.GetFrame().Result == ViconSDK::Result::Success;
}

// ----------------------------------------------------------------------------

std::vector<RigidBodyMeasurement> VICON::getRigidBodyMeasurements()
{
  std::vector<RigidBodyMeasurement> bodies;

  // get the timestamp of this data frame
  uint64_t time_ns = getCurrentTimestampNs();

  // loop through each of the tracked rigid bodies in the current frame
  size_t n = client_.GetSubjectCount().SubjectCount;
  for (size_t i=0; i<n; ++i) {
    std::string name = client_.GetSubjectName(i).SubjectName;

    // extract raw rigid body measurement and add timestamp
    bodies.push_back(getData(name));
    bodies.back().time_ns = time_ns;
  }

  return bodies;
}

// ----------------------------------------------------------------------------

uint64_t VICON::getCurrentTimestampNs()
{
  auto time = client_.GetTimecode();

  // get fps
  double fps = 0;
  switch (time.Standard) {
    case ViconSDK::TimecodeStandard::PAL:
      fps = 25.0;
      break;
    case ViconSDK::TimecodeStandard::NTSC:
      fps = 29.97;
      break;
    case ViconSDK::TimecodeStandard::NTSCDrop:
      throw std::runtime_error("NTSCDrop: invalid timecode standard");
      break;
    case ViconSDK::TimecodeStandard::Film:
      fps = 24.0;
      break;
    default:
      throw std::runtime_error("unknown: invalid timecode standard");
      break;
  }

  // base time (second resolution)
  uint64_t time_ns = (time.Seconds + 60*time.Minutes + 3600*time.Hours)*1e9;

  // subsecond resolution
  double frames = time.Frames + static_cast<double>(time.SubFrame)/time.SubFramesPerFrame;
  time_ns += static_cast<uint64_t>((frames/fps)*1e9);

  return time_ns;
}

// ----------------------------------------------------------------------------

double VICON::getTotalLatency()
{
  // GetLatencyTotal returns 0.0 if info not available
  double latency = client_.GetLatencyTotal().Total;
  return (latency == 0) ? -1 : latency;
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

RigidBodyMeasurement VICON::getData(const std::string& name)
{
  RigidBodyMeasurement body(name);
  
  // we only care about segment 0 (TODO: same as GetRootSegment?)
  constexpr unsigned int SegIdx = 0;
  std::string SegName = client_.GetSegmentName(name, SegIdx).SegmentName;

  { // position of root segment w.r.t global VICON frame
    auto data = client_.GetSegmentGlobalTranslation(name, SegName);
    body.x = data.Translation[0]*1e-3;
    body.y = data.Translation[1]*1e-3;
    body.z = data.Translation[2]*1e-3;

    // true if segment was absent in this frame
    body.occluded = data.Occluded;
  }

  { // orientation of root segment w.r.t global VICON frame
    auto data = client_.GetSegmentGlobalRotationQuaternion(name, SegName);
    body.qw = data.Rotation[3];
    body.qx = data.Rotation[0];
    body.qy = data.Rotation[1];
    body.qz = data.Rotation[2];
  }

  // get total number of markers
  body.nrMarkers = client_.GetMarkerCount(SegName).MarkerCount;

  // get number of visible markers
  body.nrVisibleMarkers = 0;
  for (size_t i=0; i<body.nrMarkers; ++i) {
    std::string mName = client_.GetMarkerName(SegName, i).MarkerName;
    Marker m(mName);

    // get position of markers w.r.t global VICON frame
    auto data = client_.GetMarkerGlobalTranslation(SegName, mName);
    m.x = data.Translation[0]*1e-3;
    m.y = data.Translation[1]*1e-3;
    m.z = data.Translation[2]*1e-3;

    // true if marker was absent in this frame
    m.occluded = data.Occluded;

    // add marker to body
    body.markers.push_back(m);

    if (!data.Occluded) body.nrVisibleMarkers++;
  }

  return body;
}

} // ns mocap
} // ns acl
