/**
 * @file optitrack.cpp
 * @brief Specialization for OptiTrack client
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 11 Dec 2019
 */

#include "mocap/client/optitrack.h"

namespace acl {
namespace mocap {

OptiTrack::OptiTrack()
{

}

// ----------------------------------------------------------------------------

} // ns mocap
} // ns acl
