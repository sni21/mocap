/**
 * @file mocap.cpp
 * @brief Broadcasts mocap data onto ROS network
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 11 Dec 2019
 */

#include "mocap/mocap.h"

namespace acl {
namespace mocap {

Mocap::Mocap(const ros::NodeHandle& nh, const ros::NodeHandle& nhp)
  : nh_(nh), nhp_(nhp)
{
  // attempt to initialize, shutdown if failed
  if (!init()) {
    ROS_ERROR("Initialization failed");
    ros::shutdown();
  }

  // attempt to initialize mocap client, shutdown if failed
  ROS_INFO_STREAM("Attempting to connect to " << client_->getName() << " server...");
  if (client_->init()) {
    ROS_INFO_STREAM("Connected!");
  } else {
    ROS_ERROR_STREAM("Could not initiate communication with " 
                      << client_->getName() << " server!");
    ros::shutdown();
  }
}

// ----------------------------------------------------------------------------

void Mocap::spin()
{
  // Do some debouncing on the number of lost packets. It is not critical
  // unless many consecutive packets are being dropped.
  constexpr int MAX_LOST_PACKETS = 4;
  unsigned int lostPackets = 0;

  while (ros::ok()) {

    // allow mocap client to receive / check for data
    if (!client_->spinOnce()) {
      if (++lostPackets >= MAX_LOST_PACKETS) {
        ROS_ERROR_STREAM_THROTTLE(1, "Did not receive data from "
                  << client_->getName() << " server! (Are cameras on?)");
      } else {
        ROS_WARN_THROTTLE(1, "Missing / dropped packets.");
      }
    } else {
      lostPackets = 0;
    }

    //
    // Process rigid bodies from mocap client
    //

    // calculate dt between mocap data frames
    static uint64_t last_time_ns = client_->getCurrentTimestampNs();
    mocap_dt_ = (client_->getCurrentTimestampNs() - last_time_ns)*1e-9;

    // skip iteration if dt is too small (occasionally happens on first iter)
    if (mocap_dt_ < 1e-6) continue;

    // Each 'enabled' rigid body will be present in this list. If the rigid
    // body is not visible, then occluded will be true and the measurements
    // will not be valid.
    auto rbMeasurements = client_->getRigidBodyMeasurements();
    for (const auto& rb : rbMeasurements) {

      // if this measurement doesn't correspond to an existing rigid body, add.
      if (bodyMap_.find(rb.name) == bodyMap_.end()) {
        ROS_WARN_STREAM("Found '" << rb.name << "'");
        bodyMap_[rb.name].reset(new Body(nh_, rb.name, bodyParams_));
      }

      // update body pose using latest mocap data
      bodyMap_[rb.name]->update(mocap_dt_, rb);

      // broadcast rigid body state/info onto ROS network
      bodyMap_[rb.name]->broadcastROS();
    }

    last_time_ns = client_->getCurrentTimestampNs();

    //
    // Remove any bodies that we didn't receive a measurement of
    //

    // NOTE: A "measurement" does not necessarily mean the object is visible.
    // Objects should only be removed from the body map if it is 'disabled' in
    // the motion capture software. In other words, invalid measurements do not
    // constitute removal from the body map.

    // For each body currently in the body map, was there a corresponding meas?
    for (auto it = bodyMap_.cbegin(); it != bodyMap_.cend(); /*manual inc*/) {
      if (std::find_if(rbMeasurements.begin(), rbMeasurements.end(),
                        [&](const RigidBodyMeasurement& rb) {
                          return rb.name == it->first;
                        }) == rbMeasurements.end())
      {
        // not found, remove from the body map
        ROS_WARN_STREAM("Removing '" << it->first << "'");
        bodyMap_.erase(it++);
      } else {
        // found, advance to next body
        ++it;
      }
    }


    // print connection information
    screenPrint();
  }
}

// ----------------------------------------------------------------------------
// Private Methods
// ----------------------------------------------------------------------------

bool Mocap::init()
{
  //
  // General mocap parameters
  //

  // what frame should everything be published w.r.t?
  nhp_.param<std::string>("tf_parent_frame", bodyParams_.parent_frame, "world");

  // allow a hidden transformation of mocap origin w.r.t ROS world origin
  std::string mocap_wrt_parent_frame;
  nhp_.param<std::string>("mocap_wrt_parent_frame",
                          mocap_wrt_parent_frame, "0 0 0 0 0 0");
  if (!parse_xyzYPR(mocap_wrt_parent_frame, bodyParams_.T_PM)) {
    ROS_ERROR_STREAM("rosparam 'mocap_wrt_parent_frame' expected format "
                      "'x y z Y P R' but got '" << mocap_wrt_parent_frame << "'");
    return false;
  }

  // allow a hidden transformation of ROS body w.r.t mocap body
  std::string body_wrt_mocap_body;
  nhp_.param<std::string>("body_wrt_mocap_body",
                          body_wrt_mocap_body, "0 0 0 0 0 0");
  if (!parse_xyzYPR(body_wrt_mocap_body, bodyParams_.T_MbB)) {
    ROS_ERROR_STREAM("rosparam 'body_wrt_mocap_body' expected format "
                      "'x y z Y P R' but got '" << body_wrt_mocap_body << "'");
    return false;
  }

  //
  // Client specific parameters and initialization
  //

  std::string client;
  nhp_.param<std::string>("client", client, "vicon");

  // make the specified client lowercase
  std::transform(client.begin(), client.end(), client.begin(),
      [](unsigned char c){ return std::tolower(c); });

  if (client == "vicon") {

    VICON::Parameters params;
    nhp_.param<std::string>("host", params.host, "192.168.0.9:801");

    client_.reset(new VICON(params));

  } else if (client == "optitrack") {

    // nh.getParam("local", localIP_);
    // nh.getParam("server", serverIP_);
    // nh.getParam("multicast_group", multicastIP_);
    // nh.getParam("command_port", commandPort_);
    // nh.getParam("data_port", dataPort_);
    // nh.getParam("pub_residuals", pubResiduals_);
    // nh.param<std::string>("topic_subname", topicSubname_, "world");

    // // client_ = std::make_unique<agile::OptiTrackClient>(localIP_, serverIP_, multicastIP_, commandPort_, dataPort_);

    // ROS_INFO_STREAM("Local address: " << localIP_);
    // ROS_INFO_STREAM("Server address: " << serverIP_);
    // ROS_INFO_STREAM("Multicast group: " << multicastIP_);
    // ROS_INFO_STREAM("Command port: " << commandPort_);
    // ROS_INFO_STREAM("Data port: " << dataPort_);

  } else {
    ROS_ERROR_STREAM("Invalid mocap client '" << client << "'.");
    return false;
  }

  return true;
}

// ----------------------------------------------------------------------------

bool Mocap::parse_xyzYPR(const std::string& xyzYPR, tf2::Transform& T) const
{
  constexpr int expectedValues = 6; // x y z Y P R
  constexpr char delim = ' ';

  std::vector<double> values;
  std::stringstream ss(xyzYPR);

  // attempt to parse the string
  try {
    std::string tmp;
    while (std::getline(ss, tmp, delim)) {
      values.push_back(std::stod(tmp));
    }
  } catch (...) {
    return false;
  }

  if (values.size() != expectedValues) {
    return false;
  }

  // input: body YPR (intrinsic 3-2-1)
  // tf2: fixed RPY (extrinsic 1-2-3)

  // create transform
  tf2::Vector3 p(values[0], values[1], values[2]);
  tf2::Quaternion q;
  q.setRPY(values[5], values[4], values[3]);
  T.setOrigin(p);
  T.setRotation(q);

  return true;
}

// ----------------------------------------------------------------------------

void Mocap::screenPrint() const
{
  const double latency = client_->getTotalLatency();

  ROS_INFO_STREAM_THROTTLE(10, "Receiving " << client_->getName()
    << " data at " << 1/mocap_dt_ << " Hz (capture latency of "
    << std::fixed << std::setprecision(2) << latency*1e3 << " ms)");
}


} // ns mocap
} // ns acl
