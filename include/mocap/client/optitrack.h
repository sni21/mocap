/**
 * @file optitrack.h
 * @brief Specialization for OptiTrack client
 * @author Parker Lusk <parkerclusk@gmail.com>
 * @date 11 Dec 2019
 */

#pragma once

#include <vector>

#include "mocap/client.h"

#include <optitrack_sdk/optitrack_client.h>

namespace acl {
namespace mocap {

  class OptiTrack : public Client
  {
  public:
    OptiTrack();
    ~OptiTrack();

    bool spinOnce() override;

    /**
     * @brief      Retrieve raw measurements of rigid bodies
     * 
     *             We assume that any body that is enabled (i.e., that could
     *             potentially be seen by mocap) will have a raw measurement
     *             in this list. For example, VICON and OptiTrack send every
     *             enabled rigid body even if the body is occluded and/or the
     *             measurement for this timestep/frame is not valid.
     *
     * @return     Rigid body raw measurements
     */
    std::vector<RigidBodyMeasurement> getRigidBodyMeasurement() override;
    
  };

} // ns mocap
} // ns acl
